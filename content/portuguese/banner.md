---
title : "Lucas Jatoba"
# full screen navigation
first_name : "Lucas"
last_name : "Jatoba"
bg_image : "images/backgrounds/lj-full-nav-bg.jpg"
# animated text loop
occupations:
- "Fotógrafo"
- "Video maker"
- "Produtor visual"

# slider background image loop
slider_images:
- "images/slider/lj-profile2.jpeg"
- "images/slider/lj-slider2.jpg"
- "images/slider/lj-slider3.jpg"

# button
button:
  enable : true
  label : "ME CONTRATE"
  link : "#contato"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""

---
