---
title : "SERVICES"
service_list:
# service item loop
- name : "Videographic production"
  image : "images/icons/web-development.png"
  
# service item loop
- name : "Photographic production"
  image : "images/icons/graphic-design.png"
  
# service item loop
- name : "Database Management"
  image : "images/icons/dbms.png"
  
# service item loop
- name : "Project Development"
  image : "images/icons/software-development.png"
  
# service item loop
- name : "Digital services"
  image : "images/icons/marketing.png"
  
# service item loop
- name : "Research Development"
  image : "images/icons/mobile-app.png"



# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---
