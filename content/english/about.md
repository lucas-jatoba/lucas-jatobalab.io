---
title : "KNOW MORE <br> ABOUT ME"
image : "images/backgrounds/lj-portrait.jpg"
# button
button:
  enable : true
  label : "Take a look at my CV"
  link : "https://br.linkedin.com/in/lucas-jatoba"

########################### Experience ##############################
experience:
  enable : true
  title : "EXPERIENCE"
  experience_list:
    # experience item loop
    - name : "Photography assistant"
      company : "http://www.thewindmillfactory.com"
      duration : "July - 2018"
      content : "A photography project during the Panorama Festival, a multi-day music festival held on Randall's Island in New York City. "
      
    # experience item loop
    - name : "Climbing Monitor and Instructor"
      company : "Centro de Escalada Limite Vertical"
      duration : "February, 2015 - May, 2018"
      content : "Instruction and monitoring, bilingual reception and climbing classes."
      
    # experience item loop
    - name : "Executive Secretary"
      company : "Conselho Municipal de Política Cultural de Niterói "
      duration : "From January to December, 2019"
      content : "Intermediary in the relationship between the council and the
      public authorities. Responsible for writing and editing documents in order
      to meet the council's needs. Development of strategies for the cultural
      growth of the city with the chairmanship of the council."

############################### Skill #################################
skill:
  enable : true
  title : "SKILL"
  skill_list:
    # skill item loop
    - name : "Photographing"
      percentage : "98%"
      
    # skill item loop
    - name : "Researching"
      percentage : "85%"
      
    # skill item loop
    - name : "Video and photo edting"
      percentage : "90%"
      
    # skill item loop
    - name : "Film directing"
      percentage : "80%"


# custom style
custom_class: "" 
custom_attributes: "" 
custom_css: ""
---

Hey! Lucas here! I am a passionate photographer, film director and life
researcher.<br>
Currently undergraduating in Antropology.
